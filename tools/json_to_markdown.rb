### Depends on: ###
#    json:				http://flori.github.com/json/									`gem install json`

require 'rubygems'
require 'json'

#If paths were specified as command line arguments, use those. Otherwise, get them from the user.
if ARGV[0] && ARGV[1]
  json_path  = ARGV[0]
  markdown_path = ARGV[1]
else
  puts "Location of JSON file to read from: "
  json_path = gets.chomp
  puts "Location of markdown file to write to: "
  markdown_path = gets.chomp
end

#Load attributes from file
attributes = JSON.parse( File.open(json_path).read )

#Open file
File.open(markdown_path, mode="w") do |file|
  file.print("---\n")
  # Set attributes
  if attributes['title'] then file.print("title: " + "\"" + attributes['title'] + "\"\n") end
  if attributes['duration'] then file.print("duration: " + "\"" + attributes['duration'] + "\"\n") end
  if attributes['description'] then file.print("description: " + "\"" + attributes['description'] + "\"\n") end
  if attributes['artist'] then file.print("artist: " + "\"" + attributes['artist'] + "\"\n") end
  if attributes['album'] then file.print("album: " + "\"" + attributes['album'] + "\"\n") end
  if attributes['year'] then file.print("year: " + "\"" + attributes['year'].to_s + "\"\n") end
  if attributes['track_number'] then file.print("track_number: " + "\"" + attributes['track_number'].to_s + "\"\n") end
  if attributes['genre'] then file.print("genre: " + "\"" + attributes['genre'] + "\"\n") end
  if attributes['copyright'] then file.print("copyright: " + "\"" + attributes['copyright'] + "\"\n") end
  if attributes['comments'] then file.print("comments: " + "\"" + attributes['comments'] + "\"\n") end
  if attributes['repository_url'] then file.print("repository_url: " + "\"" + attributes['repository_url'] + "\"\n") end
  if attributes['contact'] then file.print("contact: " + attributes['contact'] + "\n") end
  file.print("file_basename: " + "\"" + File.basename(json_path, ".json") + "\"\n")
  file.print("---\n")
end
