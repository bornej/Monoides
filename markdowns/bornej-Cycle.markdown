---
title: "Cycle"
duration: "3:24"
description: "Cycle is Monoïdes project first song"
artist: "Borne Jonathan"
album: "Monoïdes"
year: "2018"
genre: "Electro-acoustic"
copyright: "Cycle Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Monoides/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Cycle"
---
