---
title: "Nil"
duration: "3:52"
artist: "Borne Jonathan"
album: "Monoides"
year: "2018"
genre: "Electro-acoustic"
copyright: "Monoides (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Monoides/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Nil"
---
